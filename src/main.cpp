/*
  Communicate with BME280s with different I2C addresses
  Nathan Seidle @ SparkFun Electronics
  March 23, 2015

  Feel like supporting our work? Buy a board from SparkFun!
  https://www.sparkfun.com/products/14348 - Qwiic Combo Board
  https://www.sparkfun.com/products/13676 - BME280 Breakout Board

  This example shows how to connect two sensors on the same I2C bus.

  The BME280 has two I2C addresses: 0x77 (jumper open) or 0x76 (jumper closed)

  Hardware connections:
  BME280 -> Arduino
  GND -> GND
  3.3 -> 3.3
  SDA -> A4
  SCL -> A5
*/

#include <Wire.h>
#include <WiFi.h>

#include "SparkFunBME280.h"
#include "Adafruit_HTU21DF.h"

//#include "ClosedCube_SHT31D.h"
//#include "SHTSensor.h"

BME280 mySensorA; //Uses default I2C address 0x77
BME280 mySensorB; //Uses I2C address 0x76 (jumper closed)
Adafruit_HTU21DF htu = Adafruit_HTU21DF();

//ClosedCube_SHT31D sht3xd;
//SHTSensor sht;

void setup()
{
    Serial.begin(115200);
    Serial.println("Example showing alternate I2C addresses");

    Wire.begin();

    mySensorA.setI2CAddress(0x77); //The default for the SparkFun Environmental Combo board is 0x77 (jumper open).
    //If you close the jumper it is 0x76
    //The I2C address must be set before .begin() otherwise the cal values will fail to load.

    if (mySensorA.beginI2C() == false)
        Serial.println("Sensor A connect failed");

    mySensorB.setI2CAddress(0x76); //Connect to a second sensor
    if (mySensorB.beginI2C() == false)
        Serial.println("Sensor B connect failed");

    // sht3xd.begin(0x44); // I2C address: 0x44 or 0x45
    // Serial.print("Serial #");
    // Serial.println(sht3xd.readSerialNumber());
    // if (sht3xd.periodicStart(SHT3XD_REPEATABILITY_HIGH, SHT3XD_FREQUENCY_10HZ) != SHT3XD_NO_ERROR)
    //     Serial.println("[ERROR] Cannot start periodic mode");

    htu.begin();
    delay(500);

    // if (sht.init())
    // {
    //     Serial.print("init(): success\n");
    // }
    // else
    // {
    //     Serial.print("init(): failed\n");
    // }
    // //sht.setAccuracy(SHTSensor::SHT_ACCURACY_MEDIUM); // only supported by SHT3x
}

// void printResult(String text, SHT31D result)
// {
//     if (result.error == SHT3XD_NO_ERROR)
//     {
//         Serial.print(text);
//         Serial.print(": T=");
//         Serial.print(result.t);
//         Serial.print("C, RH=");
//         Serial.print(result.rh);
//         Serial.println("%");
//     }
//     else
//     {
//         Serial.print(text);
//         Serial.print(": [ERROR] Code #");
//         Serial.println(result.error);
//     }
// }

void loop()
{

    Serial.print("Temp A:");
    Serial.print(mySensorA.readTempC(), 2);
    Serial.print(" B:");
    Serial.print(mySensorB.readTempC(), 2);

    Serial.print(" C:");
    Serial.print(htu.readTemperature());

    Serial.print(" Humidity A:");
    Serial.print(mySensorA.readFloatHumidity(), 0);
    Serial.print(" B:");
    Serial.print(mySensorB.readFloatHumidity(), 0);

    Serial.print(" C:");
    Serial.print(htu.readHumidity());

    Serial.print(" Pressure A:");
    Serial.print(mySensorA.readFloatPressure(), 0);

    Serial.print(" B:");
    Serial.println(mySensorB.readFloatPressure(), 0);

    //printResult("Periodic Mode", sht3xd.periodicFetchData());

    //     if (sht.readSample()) {
    //       Serial.print("SHT:\n");
    //       Serial.print("  RH: ");
    //       Serial.print(sht.getHumidity(), 2);
    //       Serial.print("\n");
    //       Serial.print("  T:  ");
    //       Serial.print(sht.getTemperature(), 2);
    //       Serial.print("\n");
    //   } else {
    //       Serial.print("Error in readSample()\n");
    //   }

    delay(500);
}